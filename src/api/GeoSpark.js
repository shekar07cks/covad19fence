import axios from "axios";
const ApiKey = "a1b46a37c2be49dfa9a837c5b3f80645";

export const CreateGeofence = (coordinates, geometryRadius, description) => {
    const headers = {
        "Content-Type": "application/json",
        "Api-Key": ApiKey,
    };
    const body = {
        "coordinates": coordinates,
        "geometry_type": "circle",
        "geometry_radius": geometryRadius,
        "is_enabled": true,
        "description": description,
    };
    debugger
    return axios.post("https://api.geospark.co/v1/api/geofence/", body, {headers});
};

export const GetGeofenceList = (coordinates, geometryRadius) => {
    const headers = {
        "Content-Type": "application/json",
        "Api-Key": ApiKey,
    };
    return axios.get("https://api.geospark.co/v1/api/geofence/", {headers});
};

export const GetEvents = (userId, geofenceId) => {
    const headers = {
        "Content-Type": "application/json",
        "Api-Key": ApiKey,
    };
    return axios.get(`https://api.geospark.co/v1/api/event/?user_id=${userId}`, {headers});
};
