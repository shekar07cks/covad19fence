import {
  createAppContainer,
  createSwitchNavigator,
} from "react-navigation";
import HomeScreen from "../View/components/HomeComponent";
import TripScreen from "../View/components/TripComponent";

import { createStackNavigator } from "react-navigation-stack";
import {
  Dashboard,
  Permissions,
} from "../View";

const NonAuthStack = createStackNavigator(
  {
    // Login: Login,
    Dashboard: Dashboard,
    Permissions: Permissions,
    Home: HomeScreen,
    Trip: TripScreen,
  },
  {
    initialRouteName: "Permissions",
  }
);
const AppNavigator = createSwitchNavigator(
  {
    NonAuthStack: NonAuthStack,
  },
  {
    initialRouteName: "NonAuthStack",
    headerMode: "none",
  }
);

const Routes = createAppContainer(AppNavigator);

export default Routes;
