import {Dimensions} from "react-native";

const screenHeight = Math.round(Dimensions.get("window").height);
const screenWidth = Math.round(Dimensions.get("window").width);

const colors = {
  primary: "#7D60D1",
  lightPurple:"#9a78fa",
  black: "#323643",
  white: "#FFFFFF",
  gray: "#666666",
  gray1: "#9e9e9e",
  light:"#F6F6F6",
  green:"#04C900",
  yellow:"#f4c837",
  sky:"#3f80fe",
};

const sizes = {
  // global sizes
  base: 16,
  font: 14,
  radius: 6,
  padding: 20,
  smallPadding: 5,
  margin: 10,
  small: 12,

  // font sizes
  h1: 26,
  h2: 18,
  h3: 16,
  h4: 14,
  h5: 12,
  title: 18,
  header: 16,
  body: 14,
  caption: 12,
};

const fonts = {
  h1: {
    fontSize: sizes.h1,
  },
  h2: {
    fontSize: sizes.h2,
  },
  h3: {
    fontSize: sizes.h3,
  },
  header: {
    fontSize: sizes.header,
  },
  title: {
    fontSize: sizes.title,
  },
  body: {
    fontSize: sizes.body,
  },
  caption: {
    fontSize: sizes.caption,
  },
  RobotoMedium: "Roboto-Medium",
  RobotoRegular: "Roboto-Regular",
};
const fontWeight = {
  medium: {
    fontWeight:"600",
  },
  bold: {
    fontWeight:"bold",
  },
};

export {colors, sizes, fonts, screenHeight, screenWidth, fontWeight};
