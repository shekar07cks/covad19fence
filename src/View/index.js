import Dashboard from "./Dashboard";
import Permissions from "./Permissions";

export {
    Dashboard,
    Permissions,
};
