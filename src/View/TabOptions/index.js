import React from "react";
import {
    Text,
    View,
    Dimensions
} from "react-native";
import { Tab, Tabs } from "native-base";
import HomeComponent from "../components/HomeComponent";
import UserEvents from "../UserEvents";
const { height } = Dimensions.get("window");


export default class TabOptions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentTab: 0,
        };
    }

    render() {
        return (
            <View style={{ position: "absolute", height: height }}>

                <Tabs
                    tabStyle={{ backgroundColor: "#fff" }}
                    tabContainerStyle={{ elevation: 1, marginTop: 0 }}
                    tabBarUnderlineStyle={{ backgroundColor: "#2962ff", height: 2 }}
                    onChangeTab={(i) => this.setState({ currentTab: i.i }) }
                >
                    <Tab heading="Explore">
                        <HomeComponent />
                    </Tab>
                    <Tab heading="Events">
                        <UserEvents currentTab={this.state.currentTab} />
                    </Tab>
                    <Tab heading="Tab3">
                        <Text>Tab3</Text>
                    </Tab>
                    <Tab heading="Tab4">
                        <Text>Tab4</Text>
                    </Tab>
                    <Tab heading="Tab5">
                        <Text>Tab5</Text>
                    </Tab>
                    <Tab heading="Tab6">
                        <Text>Tab6</Text>
                    </Tab>
                </Tabs>
            </View>
        );
    }
}
