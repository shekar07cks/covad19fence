import React from "react";
import { Text, View, Dimensions, Animated, TouchableHighlight } from "react-native";
import TabOptions from "../TabOptions";
import SlidingUpPanel from "rn-sliding-up-panel";
import Icon from "react-native-vector-icons/FontAwesome5";
import GeoSpark from "react-native-geospark";

const { height } = Dimensions.get("window");

const styles = {
  container: {
    flex: 1,
    backgroundColor: "#f8f9fa",
    alignItems: "center",
    justifyContent: "center",
  },
  panel: {
    flex: 1,
    backgroundColor: "#3f51b5",
    position: "relative",
  },
  panelHeader: {
    // height: "100%",
    // // flex: 1,
    // backgroundColor: "#b197fc",
    // justifyContent: "flex-end",
    // padding: 24,
  },
  textHeader: {
    fontSize: 28,
    color: "#FFF",
  },
  icon: {
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: -24,
    right: 18,
    width: 48,
    height: 48,
    zIndex: 1,
  },
  iconBg: {
    backgroundColor: "#05138e",
    position: "absolute",
    top: -24,
    right: 18,
    width: 48,
    height: 48,
    borderRadius: 24,
    zIndex: 1,
  },
};

export default class SlideTab extends React.Component {
  static defaultProps = {
    draggableRange: { top: height, bottom: 180 },
  };

  _draggedValue = new Animated.Value(180);

  render() {
    const { top, bottom } = this.props.draggableRange;

    const backgoundOpacity = this._draggedValue.interpolate({
      inputRange: [height - 48, height],
      outputRange: [1, 0],
      extrapolate: "clamp",
    });

    const iconTranslateY = this._draggedValue.interpolate({
      inputRange: [height - 56, height, top],
      outputRange: [0, 56, 180 - 32],
      extrapolate: "clamp",
    });

    const textTranslateY = this._draggedValue.interpolate({
      inputRange: [bottom, top],
      outputRange: [0, 8],
      extrapolate: "clamp",
    });

    const textTranslateX = this._draggedValue.interpolate({
      inputRange: [bottom, top],
      outputRange: [0, -112],
      extrapolate: "clamp",
    });

    const textScale = this._draggedValue.interpolate({
      inputRange: [bottom, top],
      outputRange: [1, 0.7],
      extrapolate: "clamp",
    });

    return (
      <View style={styles.container}>
        {/* <Text onPress={() => this._panel.show(360)}>Show panel</Text> */}
        {this.props.children}
        <SlidingUpPanel
          ref={c => (this._panel = c)}
          draggableRange={this.props.draggableRange}
          animatedValue={this._draggedValue}
          snappingPoints={[160]}
          height={height + 180}
          friction={0.5}
        >
          <View style={styles.panel}>
          {/* <TouchableHighlight
            onPress={() => GeoSpark.startTracking()}>
            <Animated.View
              style={[
                styles.iconBg,
                {
                  opacity: backgoundOpacity,
                  transform: [{ translateY: iconTranslateY }],
                },
              ]}
            > */}
                {/* <Icon name="map-marker-alt" style={{ color: "#fff", fontSize: 25, paddingLeft: 14, paddingTop: 12 }} /> */}
                {/* </Animated.View>
                </TouchableHighlight> */}
            <View style={styles.panelHeader}>
              {/* <Animated.View
                // style={{
                //   transform: [
                //     { translateY: textTranslateY },
                //     { translateX: textTranslateX },
                //     { scale: textScale },
                //   ],
                // }}
              > */}
              <TabOptions />
                {/* <Text style={styles.textHeader}>Slidinasdasdg Up Panel</Text> */}
              {/* </Animated.View> */}
            </View>
            {/* <View style={styles.container}>
              <Text>Bottom sheet content</Text>
            </View> */}
          </View>
        </SlidingUpPanel>
      </View>
    );
  }
}
