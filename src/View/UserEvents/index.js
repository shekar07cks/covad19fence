import React from "react";
import {
    Text,
    View,
    Dimensions,
    TouchableOpacity,
} from "react-native";
import { commonstyle, theme } from "../../assets/theme";
import { Left, Right } from "native-base";
import moment from "moment";
import { GetEvents } from "../../api/GeoSpark";
import AsyncStorage from "@react-native-community/async-storage";

export default class UserEvents extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            events: [],
        };
    }

    componentWillMount() {
        this.getEventList();
    }

    // static getDerivedStateFromProps(nextProps, prevState) {
    //     debugger
    //     if (nextProps.currentTab !== prevState.currentTab) {
    //         debugger
    //         this.getEventList();
    //     }
    //   }

    getEventList = async () => {
        const userId = await AsyncStorage.getItem("geospark_userId");
        GetEvents(userId).then(res => {
            let eventsList = res.data.data.events;
            this.setState({ events: eventsList })

        }).catch(error => {
            debugger
        });
    }

    render() {
        return (
            <View style={commonstyle.p10}>
                <View style={[commonstyle.card, commonstyle.fullWidth]}>
                    <Text style={[ commonstyle.fs18, { color: theme.colors.gray }]}>Events</Text>
                    {this.state.events.length > 0 ?
                        this.state.events.map((item, i) => {
                            return (
                                <View key={i}>
                                    <View style={[commonstyle.horizontalLine, commonstyle.mt10]} />
                                    <TouchableOpacity
                                        style={[commonstyle.fDirectionRow, commonstyle.mt20, commonstyle.mb20]}>
                                        <Left>
                                            <Text style={{ color: theme.colors.black }}>{item.user_description}</Text>
                                            <Text style={[commonstyle.mt10, { color: theme.colors.gray1 }]}>{item.user_id}</Text>
                                        </Left>
                                        <Right>
                                            <Text style={[commonstyle.mt3, { color: theme.colors.green }]}>{item.event_type}</Text>
                                            <Text style={[commonstyle.mt10, { color: theme.colors.gray1 }]}>{moment(Date(item.created_at)).format("DD-MMM-YYYY-MM-SS")}</Text>
                                        </Right>
                                    </TouchableOpacity>
                                </View>
                            );
                        }
                        ) : <View style={commonstyle.mt45}>
                            <Text>No Events found</Text>
                        </View>}
                </View>
            </View>
        );
    }
}
