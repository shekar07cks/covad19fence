import React, { Component } from "react";
import {
  Platform,
  Button,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TextInput,
  TouchableHighlight,
  ActivityIndicator,
  ScrollView,
} from "react-native";
import GeoSpark from "react-native-geospark";
import { CreateGeofence } from "../../api/GeoSpark";
import Toast, { DURATION } from "react-native-easy-toast";
import AsyncStorage from "@react-native-community/async-storage";
import PushNotification from "react-native-push-notification";
const { width } = Dimensions.get("window");

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userid: "",
      description: "",
      isUserCreated: false,
      isTrakingStarted: false,
      isFetching: false,
      platforms: "ios",
      isLocationPermission: false,
      isActivityPermission: false,
      isBackgroundPermission: false,
      isMotionService: false,
      isBatteryOptimization: false,
      isMockEnabled: false,
    };
  }

  async componentWillMount() {
    const plateform = Platform.OS;
    const userId = await AsyncStorage.getItem("geospark_userId");
    const traking = await AsyncStorage.getItem("geospark_traking");
    const userDescription = await AsyncStorage.getItem("geospark_description");

    if (userId !== null) {
      const trakingStarted = traking === "yes" ? true : false;
      this.setState({
        userid: userId,
        isUserCreated: true,
        isTrakingStarted: trakingStarted,
        description: userDescription,
      });
    }

    GeoSpark.checkLocationPermission(
      status => {
        if (status == "GRANTED") {
          this.setState({
            isLocationPermission: true,
          });
        }
      },
      () => { }
    );

    if (plateform === "ios") {
      console.log("IOSSSSSSS");
      GeoSpark.checkMotionPermission(
        status => {
          console.log(status);

          if (status == "GRANTED") {
            this.setState({
              isMotionService: true,
            });
          }
        },
        () => { }
      );
    } else {
      GeoSpark.disableBatteryOptimization(this);

      GeoSpark.checkMotionPermission(
        status => {
          console.log(status);
          if (status == "ENABLED") {
            this.setState({
              isActivityPermission: true,
            });
          }
        },
        () => { }
      );

      GeoSpark.checkBackgroundLocationPermission(
        status => {
          console.log(status);
          if (status == "ENABLED") {
            this.setState({
              isBackgroundPermission: true,
            });
          }
        },
        () => { }
      );

      GeoSpark.checkLocationServices(
        status => {
          console.log(status);
          if (status == "ENABLED") {
            this.setState({
              isMotionService: true,
            });
          }
        },
        () => { }
      );

      GeoSpark.isBatteryOptimizationEnabled(
        status => {
          console.log(status);

          if (status == "ENABLED") {
            this.setState({
              isBatteryOptimization: true,
            });
          }
        },
        () => { }
      );
    }

    this.setState({
      platforms: plateform,
    });
  }

  userIDChange(e) {
    // console.log(e.nativeEvent.text);
    this.setState({
      userid: e.nativeEvent.text,
    });
  }

  updateGeofence = (e) => {
    // console.log(e.nativeEvent.text);
    this.setState({
      geoFence: e.nativeEvent.text,
    });
  }

  userDescriptionChange(e) {
    this.setState({
      description: e.nativeEvent.text,
    });
  }

  onCreateUser() {
    this.setState({
      isFetching: true,
    });
    GeoSpark.createUser(
      this.state.description,
      async success => {
        GeoSpark.toggleEvents(true, true, true, async (res) => {

          this.setState({
            isUserCreated: true,
            isFetching: false,
          });
        },
          error => {
            console.log(error);
            this.refs.toast.show("User Created Failed!");
            this.setState({
              isFetching: false,
            });
          }
        );
        const userID = success.userId;
        await AsyncStorage.setItem("geospark_userId", userID);
        await AsyncStorage.removeItem("geospark_description");
        this.setState({ userid: userID });
        // that.refs.toast.show("User Created Successfully!");
      },
      error => {
        console.log(error);
        debugger;
        this.refs.toast.show("User Created Failed!");
        this.setState({
          isFetching: false,
        });
      }
    );
  }

  createGeoFence = () => {
    debugger
    GeoSpark.getCurrentLocation(20, (success) => {
      //   var region = {
      //     longitude: success.longitude,
      //     latitude: success.latitude,
      //      // latitudeDelta: 0.0222,
      //      // longitudeDelta: 0.0152412539754657,
      //  };
      let region = [success.longitude, success.latitude];
      CreateGeofence(region, 25, this.state.geoFence).then(res => {
        debugger
        this.refs.toast.show("GeoFence Created");
      }).catch(error => {
        debugger;
      });
    }, (error) => {
      debugger;
      // do something on error
    });
  }

  onSetDescription() {
    const { description } = this.state;
    this.setState({
      isFetching: true,
    });
    GeoSpark.setDescription(
      description,
      async success => {
        console.log(success);
        await AsyncStorage.setItem("geospark_description", description);
        this.refs.toast.show("User Updated Successfully!");
        this.setState({
          isFetching: false,
        });
      },
      () => {
        this.refs.toast.show("User Updated Failed!");
        this.setState({
          isFetching: false,
        });
      }
    );
  }

  onLoginUser() {
    const { userid } = this.state;
    this.setState({
      isFetching: true,
    });
    GeoSpark.getUser(
      userid,
      async success => {
        console.log(success);
        await AsyncStorage.setItem("geospark_userId", userid);
        await AsyncStorage.removeItem("geospark_description");
        this.refs.toast.show("User Logged in Successfully!");
        this.setState({
          isUserCreated: true,
          isFetching: false,
        });
      },
      () => {
        this.refs.toast.show("User Logged in Failed!");
        this.setState({
          isFetching: false,
        });
      }
    );
  }

  onRequestLocation() {
    GeoSpark.checkLocationPermission(status => {
      if (status == "GRANTED") {
        this.setState({
          isLocationPermission: true,
        });
      } else {
        GeoSpark.requestLocationPermission();
      }
    });
  }

  onRequestActivity() {
    GeoSpark.checkMotionPermission(status => {
      if (status == "GRANTED") {
        this.setState({
          isActivityPermission: true,
        });
      } else {
        GeoSpark.requestMotionPermission();
      }
    });
  }

  onRequestBackgroundPermission() {
    GeoSpark.checkBackgroundLocationPermission(status => {
      if (status == "GRANTED") {
        this.setState({
          isBackgroundPermission: true,
        });
      } else {
        GeoSpark.requestBackgroundLocationPermission();
      }
    });
  }

  onRequestMotionORService() {
    const { platforms } = this.state;
    if (platforms === "ios") {
      GeoSpark.checkMotionPermission(async status => {
        console.log(status);
        if (status == "GRANTED") {
          this.setState({
            isMotionService: true,
          });
        } else {
          GeoSpark.requestMotionPermission();
        }
      });
    } else {
      GeoSpark.checkLocationServices(async status => {
        console.log(status);

        if (status == "ENABLED") {
          this.setState({
            isMotionService: true,
          });
        } else {
          GeoSpark.requestLocationServices();
        }
      });
    }
  }

  pushNotifications = () => {
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        debugger;
        GeoSpark.setDeviceToken("", token.token);

        console.log("TOKEN:", token);
      },

      // (required) Called when a remote or local notification is opened or received
      onNotification: function (notification) {
        console.log("NOTIFICATION:", notification);
        // process the notification here
        // required on iOS only
        // notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
      // Android only
      senderID: "348627854436",
      // iOS only
      // permissions: {
      //   alert: true,
      //   badge: true,
      //   sound: true,
      // },
      popInitialNotification: true,
      requestPermissions: true,
    });
  }



  onStartTraking() {
    // this.pushNotifications();
    debugger
    const { platforms } = this.state;
    GeoSpark.checkLocationPermission(status => {
      console.log(status);
      if (status == "GRANTED") {
        if (platforms === "ios") {
          GeoSpark.checkMotionPermission(async status => {
            console.log(status);
            if (status == "GRANTED") {
              GeoSpark.startTracking(this);
              await AsyncStorage.setItem("geospark_traking", "yes");
              this.setState({
                isTrakingStarted: true,
              });
              this.refs.toast.show("Tracking Started Successfully!");
            } else {
              GeoSpark.requestMotionPermission();
            }
          });
        } else {
          GeoSpark.checkLocationServices(async status => {
            console.log(status);
            if (status == "ENABLED") {
              GeoSpark.startTracking(this);
              this.setState({
                isTrakingStarted: true,
              });
              debugger
              GeoSpark.startListener("location", (result) => {
                debugger
                this.refs.toast.show("location update received from server");
                // do something with result.location, result.user, result.activity
            }, error => {
              debugger
            });
              await AsyncStorage.setItem("geospark_traking", "yes");
              // NativeToste.show({
              //   text: 'Wrong password!',
              //   buttonText: 'Okay'
              // });
              this.refs.toast.show("Tracking Started Successfully!");
            } else {
              GeoSpark.requestLocationServices();
            }
          });
        }
      } else {
        GeoSpark.requestLocationPermission();
      }
    });
  }

  async onStopTraking() {
    GeoSpark.stopTracking(this);
    this.setState({
      isTrakingStarted: false,
    });
    await AsyncStorage.removeItem("geospark_traking");
    this.refs.toast.show("Tracking Stopped Successfully!");
  }

  onLogout() {
    this.setState({
      isFetching: true,
    });
    GeoSpark.logout(
      async () => {
        this.setState({
          isUserCreated: false,
          isTrakingStarted: false,
          isFetching: false,
          userid: null,
        });
        await AsyncStorage.removeItem("geospark_userId");
        await AsyncStorage.removeItem("geospark_traking");
        this.refs.toast.show("User Logged out Successfully!");
      },
      () => {
        this.setState({
          isFetching: false,
        });
        this.refs.toast.show("User Logged out Failed!");
      });
  }

  static navigationOptions = {
    title: "GeoSpark Example",
  };

  render() {
    const {
      isUserCreated,
      description,
      isFetching,
      platforms,
      isBatteryOptimization,
    } = this.state;
    return (
      <View>
        {/* <Header
          // leftComponent={{ text: "GeoSpark Example", style: { color: "#fff" } }}
          centerComponent={{
            text: "GeoSpark Example",
            style: { color: "#fff" }
          }}
          // rightComponent={{ icon: "home", color: "#fff" }}
        /> */}

        <ScrollView>
          <View style={styles.container}>
            <Text style={styles.titleLabel}>User</Text>
            <TextInput
              placeholder="Enter description"
              placeholderTextColor="grey"
              underlineColorAndroid="transparent"
              style={styles.input}
              onChange={this.userDescriptionChange.bind(this)}
              value={description}
            />

            <View>
              <View style={styles.trakingContainer}>
                <View style={styles.trakingButton}>
                  <TouchableHighlight>
                    <Button
                      title="Create User"
                      disabled={isUserCreated}
                      onPress={this.onCreateUser.bind(this)}
                    />
                  </TouchableHighlight>
                </View>
                <View style={styles.trakingButton}>
                  <TouchableHighlight>
                    <Button
                      title="Set Description"
                      disabled={!isUserCreated}
                      onPress={this.onSetDescription.bind(this)}
                    />
                  </TouchableHighlight>
                </View>
              </View>
            </View>

            <View style={styles.loginContainer}>
              <TextInput
                placeholder="Enter userid"
                placeholderTextColor="grey"
                underlineColorAndroid="transparent"
                style={styles.input}
                onChange={this.userIDChange.bind(this)}
                value={this.state.userid}
              />

              {/* <Text>{isUserCreated ? "user" : "no user"}</Text> */}

              <View style={styles.logoutContainer}>
                <TouchableHighlight>
                  <Button
                    title="Get User"
                    disabled={isUserCreated}
                    onPress={this.onLoginUser.bind(this)}
                  />
                </TouchableHighlight>
              </View>
            </View>

            {/* <View style={styles.permissionContainer}>
              <Text style={styles.titleLabel}>Permission </Text>
              <View style={styles.trakingContainer}>
                <View style={styles.trakingButton}>
                  <TouchableHighlight>
                    <Button
                      title="Request Location"
                      // disabled={!isUserCreated || isLocationPermission}
                      onPress={this.onRequestLocation.bind(this)}
                    />
                  </TouchableHighlight>
                </View>
                <View style={styles.trakingButton}>
                  <TouchableHighlight>
                    <Button
                      title={
                        platforms === "ios"
                          ? "Request Motion"
                          : "Request Service"
                      }
                      // disabled={!isUserCreated || isMotionService}
                      onPress={this.onRequestMotionORService.bind(this)}
                    />
                  </TouchableHighlight>
                </View>
              </View>
            </View> */}


            {/* <View style={styles.permissionContainer}>
              <Text style={styles.titleLabel}>Android 10 Permission </Text>
              <View style={styles.trakingContainer}>
                <View style={styles.trakingButton}>
                  <TouchableHighlight>
                    <Button
                      title="Activity Permission"
                      // disabled={!isUserCreated || isActivityPermission}
                      onPress={this.onRequestActivity.bind(this)}
                    />
                  </TouchableHighlight>
                </View>
                <View style={styles.trakingButton}>
                  <TouchableHighlight>
                    <Button
                      title="Background Permission"
                      // disabled={!isUserCreated || isBackgroundPermission}
                      onPress={this.onRequestBackgroundPermission.bind(this)}
                    />
                  </TouchableHighlight>
                </View>
              </View>
            </View> */}

            {isFetching && (
              <View style={[styles.ActivityContainer, styles.horizontal]}>
                <ActivityIndicator size="large" color="#0000ff" />
              </View>
            )}

            <View style={styles.trackContainer}>
              <Text style={styles.titleLabel}>Tracking </Text>
              <View style={styles.trakingContainer}>
                <View style={styles.trakingButton}>
                  <TouchableHighlight>
                    <Button
                      title="Start Tracking"
                      // disabled={!isUserCreated || isTrakingStarted}
                      onPress={this.onStartTraking.bind(this)}
                    />
                  </TouchableHighlight>
                </View>
                <View style={styles.trakingButton}>
                  <TouchableHighlight>
                    <Button
                      title="Stop Tracking"
                      // disabled={!isUserCreated || !isTrakingStarted}
                      onPress={this.onStopTraking.bind(this)}
                    />
                  </TouchableHighlight>
                </View>
              </View>
            </View>

            <View style={styles.logoutContainer}>
              <Text style={styles.titleLabel}>Create GeoFence</Text>
              <TextInput
                placeholder="Enter GeoFence"
                placeholderTextColor="grey"
                underlineColorAndroid="transparent"
                style={styles.input}
                onChange={this.updateGeofence}
                value={this.state.geoFence}
              />
              <View style={styles.logoutContainer}>
                <TouchableHighlight>
                  <Button
                    title="GeoFence"
                    // disabled={!isUserCreated}
                    onPress={() => this.createGeoFence()}
                  />
                </TouchableHighlight>
              </View>
            </View>

            {/* {platforms !== "ios" && (
              <View style={styles.logoutContainer}>
                <Text style={styles.titleLabel}>Battery Optimization</Text>
                <View style={styles.logoutContainer}>
                  <TouchableHighlight>
                    <Button
                      title={isBatteryOptimization ? "Enable" : "Disable"}
                    // disabled="true"
                    />
                  </TouchableHighlight>
                </View>
              </View>
            )} */}

            <View style={styles.logoutContainer}>
              <Text style={styles.titleLabel}>Logout</Text>
              <View style={styles.logoutContainer}>
                <TouchableHighlight>
                  <Button
                    title="Logout"
                    // disabled={!isUserCreated}
                    onPress={this.onLogout.bind(this)}
                  />
                </TouchableHighlight>
              </View>
            </View>
            <Toast
              position={"top"}
              style={{ backgroundColor: "red" }}
              opacity={0.8}
              textStyle={{ color: "#fff" }}
              ref="toast" />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 5,
  },
  ActivityContainer: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#f2f2f2",
    zIndex: 1,
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    flex: 1,
    // padding: 10
  },
  trakingContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
  },
  trakingButton: {
    width: width * 0.45,
    height: 50,
  },
  titleLabel: {
    fontSize: 18,
    fontWeight: "bold",
    color: "black",
    padding: 5,
  },
  permissionContainer: {
    marginTop: 5,
  },
  input: {
    padding: 8,
    height: 40,
    borderColor: "grey",
    borderWidth: 1,
    borderRadius: 5,
    paddingBottom: 10,
    margin: 10,
  },
  logoutContainer: {
    flex: 1,
    marginTop: 5,
    marginLeft: 2.5,
    marginRight: 2.5,
    marginBottom: 5,
    justifyContent: "space-around",
  },
});
