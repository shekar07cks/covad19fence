import React from "react";
import {
  SafeAreaView,
} from "react-native";
import GoogleMapView from "../MapView";
import SlideTab from "../SlideTab";
import GeoSpark from "react-native-geospark";
import PushNotification from "react-native-push-notification";

export default class Dashboard extends React.Component {
  static navigationOptions = {
    header: null,
};
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentWillMount(){
    // GeoSpark.setDeviceToken(this, "FCM DeviceToken")
    this.pushNotifications()
  }

  pushNotifications = () => {
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function (token) {
        debugger
        // GeoSpark.setDeviceToken("", token.token);
        
        console.log("TOKEN:", token);
      },

      // (required) Called when a remote or local notification is opened or received
      onNotification: function (notification) {
        console.log("NOTIFICATION:", notification);
        // process the notification here
        // required on iOS only
        // notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
      // Android only
      senderID: "348627854436",
      // iOS only
      // permissions: {
      //   alert: true,
      //   badge: true,
      //   sound: true,
      // },
      popInitialNotification: true,
      requestPermissions: true,
    });
  }

  render() {
    return <>
      {/* <SafeAreaView> */}
          <SlideTab >
          <GoogleMapView />
          </SlideTab>
      {/* </SafeAreaView> */}
    </>;
  }
}


