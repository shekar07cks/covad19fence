import React from "react";
import {
    SafeAreaView,
    View,
    StyleSheet,
    Dimensions,
    TouchableHighlight,
    Animated
} from "react-native";
import GeoSpark from "react-native-geospark";
import MapView from "react-native-maps";
import Icon from "react-native-vector-icons/FontAwesome5";
import CustomMapStyle from "./customMapStyle.json";
import { theme } from "../../assets/theme";
import Geolocation from "@react-native-community/geolocation";
import Toast, { DURATION } from "react-native-easy-toast";
import AsyncStorage from "@react-native-community/async-storage";
import { GetGeofenceList } from "../../api/GeoSpark";

const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE = 24.9180;
const LONGITUDE = 67.0971;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const origin = { latitude: 37.3318456, longitude: -122.0296002 };


export default class GoogleMapView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userid: null,
            description: "chandra shekhar",
            isUserCreated: false,
            isTrakingStarted: false,
            isFetching: false,
            platforms: "ios",
            isLocationPermission: false,
            isActivityPermission: false,
            isBackgroundPermission: false,
            isMotionService: false,
            isBatteryOptimization: false,
            isMockEnabled: false,
            geofenceList: [],

            coordinate: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                longitudeDelta: LONGITUDE_DELTA,
                latitudeDelta: LATITUDE_DELTA,
            },
        };
    }

    componentWillMount() {
        this.getCurrentLocation();
        // this.onCreateUser();
        this.geofenceList();
    }

    componentDidMount(){
        setInterval(() => {
            // this.geofenceList();
            GeoSpark.updateCurrentLocation(100);
        }, 5000);
    }

    geofenceList = () => {
        GetGeofenceList().then(res => {
            let geofenceList = res.data.data.geofences;
            this.setState({ geofenceList });
            debugger;
        }).catch(error => {
            debugger;
        });
    }


    getCurrentLocation = () => {
        GeoSpark.getCurrentLocation(100, (success) => {
            // do something on success
            var initialRegion = {
                latitude: success.latitude,
                longitude: success.longitude,
                latitudeDelta: 0.0222,
                longitudeDelta: 0.0102412539754657,
            };
            this.map.animateToRegion({
                latitude: success.latitude,
                longitude: success.longitude,
                latitudeDelta: 0.0222,
                longitudeDelta: 0.0102412539754657,
            }, 1000);
            this.refs.toast.show("locaton updated Successfully!");
            this.setState({ coordinate: initialRegion });
        }, (error) => {
            debugger;
            // do something on error
        });
    }

    // getCurrentLocation = async () => {
    //     Geolocation.getCurrentPosition((position) => {
    //         // console.warn(position, "res postion");
    //         var lat = parseFloat(position.coords.latitude);
    //         var long = parseFloat(position.coords.longitude);

    //         var initialRegion = {
    //                         latitude: lat,
    //                         longitude: long,
    //                         latitudeDelta: 0.0222,
    //                         longitudeDelta: 0.0102412539754657,
    //                     };
    //                     this.map.animateToRegion({
    //                         latitude: lat,
    //                         longitude: long,
    //                         latitudeDelta: 0.0222,
    //                         longitudeDelta: 0.0102412539754657,
    //                     }, 1000);
    //                     this.refs.toast.show("locaton updated Successfully!");
    //                     this.setState({ coordinate: initialRegion });

    //     }).catch(error => {
    //         console.warn(error, "res postion error");
    //     }, { enableHighAccuracy: true, timeout: 10000 });
    // }


    // onCreateUser = () => {
    //     // let that = this;
    //     // this.setState({
    //     //     isFetching: true,
    //     // });
    //     GeoSpark.createUser(
    //         this.state.description,
    //         async success => {
    //             GeoSpark.toggleEvents(true, true, true, async (res) => {
    //                 // this.onStartTraking();
    //                 this.refs.toast.show("user created Successfully!");
    //                 this.setState({
    //                     isUserCreated: true,
    //                     isFetching: false,
    //                 });
    //             },
    //                 error => {
    //                     console.log(error);
    //                     that.refs.toast.show("User Created Failed!");
    //                     this.setState({
    //                         isFetching: false,
    //                     });
    //                 }
    //             );
    //             const userID = success.userId;
    //             await AsyncStorage.setItem("geospark_userId", userID);
    //             await AsyncStorage.removeItem("geospark_description");
    //             this.setState({ userid: userID });
    //             this.refs.toast.show("User Created Successfully!");
    //         },
    //         error => {
    //             console.log(error);
    //             this.refs.toast.show("User Created Failed!");
    //             this.setState({
    //                 isFetching: false,
    //             });
    //         }
    //     );
    // }

    // onStartTraking() {
    //     debugger
    //     let that = this;
    //     const { platforms } = this.state;
    //     GeoSpark.checkLocationPermission(status => {
    //       console.log(status);
    //       debugger
    //       if (status == "GRANTED") {
    //         debugger
    //           GeoSpark.checkLocationServices(async status => {
    //             console.log(status);
    //             if (status == "ENABLED") {
    //                 debugger
    //               GeoSpark.startTracking(this);
    //               this.setState({
    //                 isTrakingStarted: true
    //               });
    //               await AsyncStorage.setItem("geospark_traking", "yes");
    //             //   that.refs.toast.show("Tracking Started Successfully!");
    //             } else {
    //               GeoSpark.requestLocationServices();
    //             }
    //           });

    //       } else {
    //         GeoSpark.requestLocationPermission();
    //       }
    //     });
    //   }
    _draggedValue = new Animated.Value(180);

    render() {
        // const top: height;
        const bottom = 180;
        const backgoundOpacity = this._draggedValue.interpolate({
            inputRange: [height - 48, height],
            outputRange: [1, 0],
            extrapolate: "clamp",
          });
      
          const iconTranslateY = this._draggedValue.interpolate({
            inputRange: [height - 56, height, height],
            outputRange: [0, 56, 180 - 32],
            extrapolate: "clamp",
          });
        return <View style={styles.container}>
            <MapView
                style={styles.map}
                // showsTraffic={true}
                customMapStyle={CustomMapStyle}
                // onUserLocationChange={{coordinates:myLocation}}
                // showsUserLocation={true}
                followsUserLocation={true}
                zoomEnabled={true}
                zoomControlEnabled={true}
                showsScale={true}
                onLayout={this.onLayout}
                // onMapReady={() => this.fitAllMarkers()}
                minZoomLevel={5}
                // region={this.state.coordinate}
                ref={map => (this.map = map)}
                // loadingEnabled={true}
                onRegionChange={this.onRegionChange}
            // onLayout = {() => { this.map.fitToCoordinates([{ latitude: 13.014452, longitude:77.6426543 }, { latitude: 13.000813, longitude:77.626954 }],  { edgePadding: { top: 0, right: 50, bottom: 50, left: 10 }, animated: true, }); } }
            >
                {this.state.geofenceList.map(i => {
                    let coordinate = {
                        latitude: i.geometry_center.coordinates[1],
                        longitude: i.geometry_center.coordinates[0],
                        longitudeDelta: 0.0102412539754657,
                        latitudeDelta: 0.0222,
                    }

                    return <MapView.Circle
                        key={(coordinate + coordinate).toString()}
                        center={coordinate}
                        radius={25}
                        strokeWidth={1}
                        strokeColor={"#1a66ff"}
                        fillColor={"rgba(230,238,255,0.5)"}
                    // onRegionChangeComplete = { this.onRegionChangeComplete.bind(this) }
                    />
                })}
                <MapView.Marker
                    tracksViewChanges={false}
                    coordinate={this.state.coordinate}
                    title={"title"}
                    description={"description"}>
                    <Icon name="map-marker-alt" style={{ color: theme.colors.primary, fontSize: 20 }} />
                </MapView.Marker>
            </MapView>
            <TouchableHighlight
                onPress={() => {
                    this.getCurrentLocation();
                    this.geofenceList();
                }}
                style={styles.iconBg}
                >
                {/* <View
                    style={[
                        styles.iconBg
                    ]}
                > */}
                    <Icon name="map-marker-alt" style={{color: "#fff", fontSize: 25, paddingLeft: 14, paddingTop: 12 }} />
                {/* </View> */}
            </TouchableHighlight>

            <Toast ref="toast" />
        </View>;
    }
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: "100%",
        width: "100%",
        justifyContent: "flex-end",
        alignItems: "center",
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    iconBg: {
        backgroundColor: "#05138e",
        position: "absolute",
        top: 10,
        right: 18,
        width: 48,
        height: 48,
        borderRadius: 24,
        zIndex: 1,
      },
});


