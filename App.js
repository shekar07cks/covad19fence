import React, { Component, Fragment } from "react";
import {
  SafeAreaView,
  StatusBar,
  NativeAppEventEmitter,
  YellowBox,
} from "react-native";
// NativeAppEventEmitter;
import Routes from "./src/navigation/AppNavigator";
// import { Provider } from "react-redux";
// import Store from "./src/store/store";
import SplashScreen from "react-native-splash-screen";

// YellowBox.ignoreWarnings(["Warning: ReactNative.createElement"]);

export default class App extends Component {
  componentDidMount() {
    // do stuff while splash screen is shown
    // After having done stuff (such as async tasks) hide the splash screen
    SplashScreen.hide();
  }
  render() {

    return (
      <Fragment>
        {/* <Provider store={Store}> */}
          <StatusBar backgroundColor="rgb(125,96,209)" barStyle="light-content" />
          <Routes />
        {/* </Provider> */}
      </Fragment>
    );
  }
}

